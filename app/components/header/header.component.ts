import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public logoName: string;
  public homeUrl: string;
  public pageUrl: string;
  public about: string;
  constructor() {
    this.logoName = 'Monsters of Tibia';
    this.homeUrl = 'Home';
    this.pageUrl = 'Search';
    this.about = 'About';
   }

  ngOnInit(): void {
  }

}