import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  public logo: string;
  public description: string;
  constructor() {
    this.logo = '../../../assets/img/tibia.png';
    this.description = 'Creatures are the inhabitants of Tibia aside from human players and NPCs. Every different type creature has its own characteristics which make it unique. Creatures share the entire world of Tibia, so they can be found all throughout the land. They will generally stay with other creatures of their own kind, and different creatures prefer different environments. This is why you can find Deer and Rabbits in grassy places. Slimes will always be found near water (usually underground), Dwarves spend most of their time in the mines and Demons prefer places deep underground. Usually, the most dangerous places are the hardest to reach. When creatures die they leave a Corpse.';
   }

  ngOnInit(): void {
  }

}
