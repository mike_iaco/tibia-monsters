import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footerOne: string;
  public footerButtonOne: string;
  public footerButtonSecond: string;
  public footerButtonThird: string;
  constructor() {
    this.footerOne = 'Copyright by Victoria & Mike. All rights reserved.';
    this.footerButtonOne = 'About CipSoft';
    this.footerButtonSecond = 'Service Agreement';
    this.footerButtonThird = 'Privacy Policy';
   }

  ngOnInit(): void {
  }

}
